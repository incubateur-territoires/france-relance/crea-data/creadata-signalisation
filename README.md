![](https://git.atd16.fr/cesig/gestion-de-git/-/raw/master/img/logo_geo16crea.png)


# **Mise en place de la signalisation**

<img src="ressources/illustration.jpeg" width="844" height="562.5" />

Ce projet recense l'ensemble des fichiers permettant la mise en place d'une base de données de gestion de la signalisation verticale et part la suite horizontale.

Ce modèle est mis en oeuvre par l'ATD16 dans le cadre d'une utilisation sur le logiciel X'MAP développé par la société SIRAP. Il s'implémente dans un serveur de base de base de données PostgreSQL/PostGIS. 
Les scripts pour la sémiologie sont en sld qui sont lu par geosever pour visualiser les différentes vue en flux.
Néanmoins il peut être adapté pour un usage sur un autre outil.

Il est basé sur l'inventaire des supports et des panneaux, leurs entretiens.


# **Principe de fonctionnement**

Une table géographique permet d'inventorier les supports des panneaux.

Des tables non-géographiques pour chaque type de panneau(Police, Directionnel, information local, vigilance et adresse). Elles permettent de renseigner plusieurs informations comme la taille, la classe de rétroréfléchissement, le coût, l'entretien... 
Ces tables sont ratachées à la table des supports via l'identifiant des supports.

# **Génèse : quelques dates**

* 2020 : *Réflection et premier devéllopement suite à la demande de commune charentaise pour recenser leur patrimoine de signalisation.*
* 2021 : *Dévellopement plus pousser avec notamment la sémiologie.*
* Mars 2022 : *Phase de test pour des communes charentaises*
* Mai 2022 : *Développement des améliorations suite aux retours des communes*
* Septembre 2022 : *Mise en place des améliorations en production*


# **Ressources du projet**

* MCD [[pdf](donnees/bdd/xmap/Mcd/signalisation.pdf)]
* Schéma applicatif [[pdf](ressources/Sch%C3%A9ma_applicatif_signalisation_verticale.pdf)]
* Script agrégé de l'ensemble de la structure[[sql]()]
* Schéma **atd16_signalisation** [[sql](donnees/bdd/xmap/000_create_schema_atd16_signalisation.sql)]
    * Fonctions-triggers génériques [[sql](donnees/bdd/xmap/001_create_function_trigger_generique.sql)]
    * Tables de listes déroulantes :
         * Table **lst_type_support** [[sql](donnees/bdd/xmap/010_lst_type_support.sql)]
         * Table **lst_fleche** [[sql](donnees/bdd/xmap/012_lst_fleche.sql)]
         * Table **lst_statut** [[sql](donnees/bdd/xmap/013_lst_statut.sql)]
         * Table **lst_nature** [[sql](donnees/bdd/xmap/014_lst_nature.sql)]
         * Table **lst_principal** [[sql](donnees/bdd/xmap/015_lst_principal.sql)]
         * Table **lst_etat_entretien** [[sql](donnees/bdd/xmap/016_lst_etat_entretien.sql)]
         * Table **lst_type_police** [[sql](donnees/bdd/xmap/018_lst_type_police.sql)]
         * Table **lst_type_directionnel** [[sql](donnees/bdd/xmap/019_lst_type_directionnel.sql)]
         * Table **lst_type_inf_local** [[sql](donnees/bdd/xmap/020_lst_type_inf_local.sql)]
         * Table **lst_section_support** [[sql](donnees/bdd/xmap/021_lst_section_support.sql)]
         * Table **lst_typologie_support** [[sql](donnees/bdd/xmap/022_lst_typologie_support.sql)]
         * Table **lst_dimension_support** [[sql](donnees/bdd/xmap/023_lst_dimension_support.sql)]
         * Table **lst_classe_retroreflechissant** [[sql](donnees/bdd/xmap/024_lst_classe_retroreflechissant.sql)]
         * Table **lst_taille_panneau** [[sql](donnees/bdd/xmap/025_lst_taille_panneau.sql)]
    * Tables non géographiques des panneaux des communes :
        * Table **ngeo_panneau_info_local_com** [[sql](donnees/bdd/xmap/030_ngeo_panneau_info_local_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/130_trigger_panneau_info_local_com.sql)]
        * Table **ngeo_panneau_police_com** [[sql](donnees/bdd/xmap/040_ngeo_panneau_police_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/140_trigger_panneau_police_com.sql)]
        * Table **ngeo_panneau_directionnel_com** [[sql](donnees/bdd/xmap/050_ngeo_panneau_directionnel_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/150_trigger_panneau_directionnel_com.sql)]
        * Table **ngeo_panneau_vigilance_com** [[sql](donnees/bdd/xmap/060_ngeo_panneau_vigilance_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/160_trigger_panneau_vigilance_com.sql)]
        * Table **ngeo_panneau_adresse_com** [[sql](donnees/bdd/xmap/070_ngeo_panneau_adresse_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/170_trigger_panneau_adresse_com.sql)]
    * Tables non géographiques des panneaux des communauté de communes :
        * Table **ngeo_panneau_info_local_cdc** [[sql](donnees/bdd/xmap/031_ngeo_panneau_info_local_cdc.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/131_trigger_panneau_info_local_cdc.sql)]
        * Table **ngeo_panneau_police_cdc** [[sql](donnees/bdd/xmap/041_ngeo_panneau_police_cdc.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/141_trigger_panneau_police_cdc.sql)]
        * Table **ngeo_panneau_directionnel_cdc** [[sql](donnees/bdd/xmap/051_ngeo_panneau_directionnel_cdc.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/151_trigger_panneau_directionnel_cdc.sql)]
        * Table **ngeo_panneau_vigilance_cdc** [[sql](donnees/bdd/xmap/061_ngeo_panneau_vigilance_cdc.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/161_trigger_panneau_vigilance_cdc.sql)]
        * Table **ngeo_panneau_adresse_cdc** [[sql](donnees/bdd/xmap/071_ngeo_panneau_adresse_cdc.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/171_trigger_panneau_adresse_cdc.sql)]
    * Tables géographiques des communes :
        * Table **geo_support_signalisation_com** [[sql](donnees/bdd/xmap/080_geo_support_signalisation_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/180_trigger_geo_support_signalisation_com.sql)]
    * Tables géographiques des communauté de communes :
        * Table **geo_support_signalisation_cdc** [[sql](donnees/bdd/xmap/081_geo_support_signalisation_cdc.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/181_trigger_geo_support_signalisation_cdc.sql)]
* Représentation graphique et thématique du projet
    * Représentation graphique **des supports** [[json](donnees/representation_graphique/010_support_xmap.json)]
    * Représentation thématique **des panneaux directionnel** [[sld](donnees/representation_graphique/001_panneau_directionnel.sld)]
    * Représentation thématique **des panneaux d'information local** [[sld](donnees/representation_graphique/002_panneau_info_local.sld)]
    * Représentation thématique **des panneaux de police** [[sld](donnees/representation_graphique/003_panneau_police.sld)]
     * Représentation thématique **des panneaux de vigilance** [[sld](donnees/representation_graphique/004_panneau_vigilance.sld)]
    * Représentation thématique **des panneaux d'adresse** [[sld](donnees/representation_graphique/005_panneau_adresse.sld)]
    * Représentation graphique **des supports sans panneau** [[sld](donnees/representation_graphique/006_V_support_ss_pann.sld)]
    * Représentation graphique **des supports avec 2 types de panneaux** [[sld](donnees/representation_graphique/007_v_geo_support_panneau_2_type.sld)]
    * Représentation graphique **des supports avec 2 panneaux principaux** [[sld](donnees/representation_graphique/008_v_geo_support_2_panneaux_principaux.sld)]
    * Représentation thématique **des classes rétroréfléchissantes des panneaux** [[sld](donnees/representation_graphique/009_classe_retroreflichissante.sld)]
    * Représentation graphique **des panneaux dans un bon état** [[sld](donnees/representation_graphique/012_entretien_panneau_bon.sld)]
    * Représentation graphique **des panneaux dans un état moyen** [[sld](donnees/representation_graphique/013_entretien_panneau_moyen.sld)]
    * Représentation graphique **des panneaux dans un état mauvais** [[sld](donnees/representation_graphique/014_entretien_panneau_mauvais.sld)]
    * Représentation graphique **des panneaux dont l'état n'est pas connu** [[sld](donnees/representation_graphique/015_entretien_panneau_non_connu.sld)]

# Ressources annexes
* Notice d'utilisation dans X'MAP [[pdf]()]
* Icon des panneaux [[zip](ressources/Icon%20panneaux/Bibliotheque_panneaux.zip)]


# **Ressources générales**

* [Les bonnes pratiques dans GitLab](https://git.atd16.fr/cesig/gestion-de-git/-/blob/master/les_bonnes_pratiques_gitlab.md)

* Liste des panneaux de signalisation [[pdf](http://www.msr83.fr/IMG/pdf/catalogue_des_signaux_routiers.pdf)]

* Taille et classe des panneaux [[lien](https://fr.wikipedia.org/wiki/Panneau_de_signalisation_routi%C3%A8re_en_France)]

