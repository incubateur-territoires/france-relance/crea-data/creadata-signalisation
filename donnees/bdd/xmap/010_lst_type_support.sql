-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/23 : DC / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table non géographique : Domaine de valeur des types de support pour les panneaux                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_type_support
(
 
   gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement 
   code integer, --[ATD16]Code du support
  libelle character varying(255), --[ATD16]Valeur du support
  "order" integer,  --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_typ_support PRIMARY KEY (gid)
 
)
WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_type_support
  OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_type_support TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_type_support(code, libelle,"order")
	VALUES
('00', 'Simple mât','1'),
('01', 'Double mâts','2'),
('02', 'Façade','3'),
('03', 'Mur','4'),
('04', 'Sol','5');