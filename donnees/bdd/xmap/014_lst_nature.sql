-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/30 : DC / Création de la table sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table non géographique : Domaine de valeur des natures des panneaux                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_nature
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
  code integer, --[ATD16]Code de la nature du panneau
  libelle character varying(255) ,--[ATD16]Valeur de la nature du panneau
  "order" integer, --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_nature PRIMARY KEY (gid)
)
WITH
(
OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_nature 
  OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_nature   TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_nature   (code, libelle, "order")
VALUES
('00','','1'),
('01','Permanent','2'),
('02','Temporaire','3');