-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/30 : DC / Création de la table sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                     Table non géographique : Domaine de valeur des types de panneau d'information local                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_type_inf_local
(
	gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
	code integer,--[ATD16] Code du panneau d'information local   
	type character varying (100), --[Norme Nationale] Type de panneau d'information local   
	code_pann character varying (10), --[Norme Nationale] Code du panneau d'information local   
	libelle character varying(500), --[Norme Nationale] Nom du panneau d'information local   
	"order" integer,  --[ATD16] ordre des valeurs
	CONSTRAINT pk_lst_type_inf_local PRIMARY KEY (gid)
)
WITH
(
OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_type_inf_local
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_type_inf_local TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_type_inf_local (code, type, code_pann,libelle,"order")
VALUES
('00', '','','','1'),
('1','Panneaux de signalisation d''information locale','DC43','Annonce les services et équipements desservis','2'),
('2','Panneaux de signalisation d’information locale','Dc29','Endroit où l''usager doit commencer sa manoeuvre pour se diriger vers les services','3');	