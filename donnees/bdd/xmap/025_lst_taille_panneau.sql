-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/06 : DC / Création de la table

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table liste : table listes de la taille des panneaux								                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_taille_panneau 
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
  code integer, --[ATD16]Code de la classe
  libelle character varying(255),  --[ATD16]Valeur de la classe
  "order" integer,  --[ATD16] ordre des valeurs
  CONSTRAINT pk_taille_panneau   PRIMARY KEY (gid)
)
WITH 
(
  OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_taille_panneau
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_taille_panneau TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_taille_panneau (code, libelle,"order")
VALUES
('01','Miniature','1'),
('02','Petite','2'),
('03','Normale','3'),
('04','Grande','4'),
('05','Très grande','5');