-- ################################################################# SUIVI CODE SQL #################################################################

-- 2025/05/04 : DC / Création de la table sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table non géographique : Domaine de valeur des dimensions des supports			                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_dimension_support 
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
  code integer, --[ATD16]Code de la typologie
  code_sup character varying(3),--[ATD16]Code qui permet de faire une liste liée avec la table des sections
  forme character varying(20), --[ATD16]forme du suport pour liée la table à la table lst_section_support 
  libelle character varying(255),  --[ATD16]Valeur de la typologie
  "order" integer,  --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_dimension_support  PRIMARY KEY (gid)
)
WITH 
(
  OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_dimension_support
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_dimension_support TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_dimension_support (code,code_sup, forme, libelle,"order")
VALUES
('00','1','Rond lisse','48,3mm','1'),
('01','1','Rond lisse','60,3mm','2'),
('02','1','Rond lisse','76mm','3'),
('03','1','Rond lisse','89mm','4'),
('04','1','Rond lisse','114mm','5'),
('05','1','Rond lisse','140mm','6'),
('06','1','Rond lisse','168mm','8'),
('07','1','Rond lisse','194mm','9'),
('08','1','Rond lisse','219mm','10'),
('09','2','Rond cannelé','48,3mm','11'),
('10','2','Rond cannelé','60,3mm','12'),
('11','2','Rond cannelé','76mm','13'),
('12','2','Rond cannelé','89mm','14'),
('13','2','Rond cannelé','114mm','15'),
('14','2','Rond cannelé','140mm','16'),
('15','2','Rond cannelé','168mm','17'),
('16','2','Rond cannelé','194mm','18'),
('17','2','Rond cannelé','219mm','19'),
('18','3','Carré','40 × 40 (en mm)','20'),
('19','3','Carré','80 × 80 (en mm)','21'),
('20','4','Rectangle','40 × 27 (en mm)','22'),
('21','4','Rectangle','80 × 40 (en mm)','23'),
('22','5','En IPN','57 × 95 (en mm)','24'),
('23','5','En IPN','70 × 117 (en mm)','25'),
('24','5','En IPN','87 × 145 (en mm)','26'),
('25','5','En IPN','107 × 178 (en mm)','27'),
('26','5','En IPN','131 × 220 (en mm)','28'),
('99','99','Autre','','99');