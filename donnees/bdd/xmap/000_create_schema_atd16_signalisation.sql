-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/30 : DC / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                         SCHEMA : Gestion de la signalisation verticale des communes                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE SCHEMA atd16_signalisation;
ALTER SCHEMA atd16_signalisation OWNER TO sditecgrp;
COMMENT ON SCHEMA atd16_signalisation IS 'Gestion de la signalisation verticale des communes  ';