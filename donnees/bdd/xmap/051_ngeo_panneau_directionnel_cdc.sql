-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/17 : DC / Création de la table sur Git et des tiggers datemaj et date création
-- 2021/08/18 : DC Création du trigger d import du code insee
-- 2022/09/15 : DC Ajout du champs panneau_type
-- 2022/09/16 : DC / Ajout des commentaires
--##################################################################################################################################################
-- ###                                                                                                                                         ###
-- ###                              Table non géographique : des panneaux directionnel/cdc                                                     ###
-- ###                                                                                                                                         ###
--##################################################################################################################################################

CREATE TABLE atd16_signalisation.ngeo_panneau_directionnel_cdc
(
	gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
	insee character varying(6), --[ATD16] code insee de la commune
	id_support integer, --[ATD16] Identifiant du support du panneau
	type_pann integer, --[ATD16] Liste deroulante lst_type_directionnel, code du panneau
	statut integer, --[ATD16] Liste deroulante lst_statut, code du statut
	panneau_principal integer, --[ATD16] Liste deroulante lst_principal, code de la valeur
	ident character varying(255), --[ATD16] Numéro du panneau
	nature integer, --[ATD16] Liste deroulante lst_nature, code de la nature
	taille integer, --[ATD16] Liste deroulante de la table lst_taille qui signifie la taille du panneau
	retroreflechissant integer, --[ATD16]Le panneau est il retroreflechissan
	classe_reflechissante integer, --[ATD16] Liste déroulante de la table classe retroreflechissant 
	date_pose date, --[ATD16] Date de pose du panneau
	fleche integer, --[ATD16] Liste deroulante lst_fleche, code du sens de la fleche
	etat_entretien integer, --[ATD16] Liste deroulante lst_etat_entretien, code de l'état
	date_etat date,--[ATD16] Date à laquelle l'état est notifié
	gestionnaire character varying(45),--[ATD16] Gestionnaire du panneau
	fournisseur character varying(45),--[ATD16] Fourb=nisseur du panneau
	cout character varying(10),--[ATD16] prix d'achat du panneau
	observations character varying(255),--[ATD16] Diverses observations
	origdata character varying(255), --[ATD16] Origine de la donnée
	panneau_type character varying(50) DEFAULT --[ATD16]Mets par defaut le type de panneau va servir pour la vue des qui rassemble tout les panneaux
	date_creation timestamp without time zone, --[ATD16] Date de création du panneau
	date_maj timestamp without time zone,--[ATD16] Date de mise à jour du panneau
	CONSTRAINT pk_ngeo_panneau_directionnel_cdc PRIMARY KEY (gid)
)
WITH 
(
OIDS=FALSE
);
ALTER TABLE atd16_signalisation.ngeo_panneau_directionnel_cdc
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.ngeo_panneau_directionnel_cdc TO sditecgrp;

-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_signalisation.ngeo_panneau_directionnel_cdc IS '[ATD16] Table non géographique listant les indormations des panneaux de type directionnel';

COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.gid IS '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.insee IS '[ATD16] code insee de la commune';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.id_support IS '[ATD16] Identifiant du support du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.type_pann IS '[ATD16] Liste deroulante lst_type_inf_local, code du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.statut IS '[ATD16] Liste deroulante lst_statut, code du statut';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.panneau_principal IS '[ATD16] Numéro du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.ident IS '[ATD16] Numéro du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.nature IS '[ATD16] Liste deroulante lst_nature, code de la nature';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.taille IS '[ATD16] Liste deroulante de la table lst_taille qui signifie la taille du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.retroreflechissant IS '[ATD16]Le panneau est il retroreflechissant';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.classe_reflechissante IS '[ATD16] Liste déroulante de la table classe retroreflechissant'; 
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.date_pose IS '[ATD16]Date de pose du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.fleche IS '[ATD16] Liste deroulante lst_fleche, code du sens de la fleche';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.etat_entretien IS '[ATD16] Liste deroulante lst_etat_entretien, code de l''état';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.date_etat IS '[ATD16] Date à laquelle l''état est notifié';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.gestionnaire IS '[ATD16] Gestionnaire du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.fournisseur IS '[ATD16] Fourb=nisseur du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.cout IS '[ATD16] prix d''achat du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.observations IS '[ATD16] Diverses observations';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.origdata IS '[ATD16] Origine de la donnée';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.panneau_type IS '[ATD16] Mets par defaut le type de panneau va servir pour la vue des qui rassemble tout les panneaux';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.date_creation IS '[ATD16] Date de création du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_directionnel_cdc.date_maj IS '[ATD16] Date de mise à jour du panneau';