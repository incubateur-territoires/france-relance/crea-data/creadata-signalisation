-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/30 : DC / Création de la table sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                            Table non géographique : Domaine de valeur du sens des flèches                                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_fleche 
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement 
  code integer, --[ATD16]Code du sens de la fléche
  libelle character varying(255) ,--[ATD16]Valeur du sens de la flèche
  "order" integer, --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_fleche PRIMARY KEY (gid)
)
WITH 
(
  OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_fleche 
  OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_fleche  TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_fleche  (code, libelle, "order")
VALUES
('00','','1'),
('01','Aucune','2'),
('02','Gauche','3'),
('03','Droite','4'),
('04', 'Tout droit','5'),
('05','Gauche et Droite','6');