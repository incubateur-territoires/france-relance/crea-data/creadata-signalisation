-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/17 : DC / Création de la table sur Git et des tiggers datemaj et date création
-- 2022/09/27 : DC Création du trigger pour le champ type panneau

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                      Fonctions triggers et triggers spécifiques à la table ngeo_panneau_vigilance /cdc                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- ##################################################################################################################################################
-- ###                                      Fonction de supression des panneaux de vigilance suite à la supression du support                     ###
-- ##################################################################################################################################################

-- #################################################################### Fonction ####################################################################

CREATE OR REPLACE FUNCTION  atd16_signalisation.f_delete_pan_vigilance_cdc ()

RETURNS trigger AS

$BODY$ 

 BEGIN
 DELETE FROM atd16_signalisation.ngeo_panneau_vigilance_cdc 
 WHERE (id_support) = (OLD.gid) ;
RETURN NEW;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION atd16_signalisation.f_delete_pan_vigilance_cdc ()
  OWNER TO sditecgrp;
GRANT EXECUTE ON FUNCTION atd16_signalisation.f_delete_pan_vigilance_cdc() TO public;
GRANT EXECUTE ON FUNCTION atd16_signalisation.f_delete_pan_vigilance_cdc() TO sditecgrp;

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER d_b_pan_vigilance_cdc
    AFTER DELETE
    ON atd16_signalisation.geo_support_signalisation_cdc 
    FOR EACH ROW
    EXECUTE FUNCTION atd16_signalisation.f_delete_pan_vigilance_cdc();

-- ##################################################################################################################################################
-- ###                                                     Création de la donnée pour le champ type_panneau                                       ###
-- ##################################################################################################################################################

-- #################################################################### Fonction ####################################################################

-- Fonction qui indique dans la table de quel type de panneau il s'agit. Et cette valeur sera utile dans la vue v_geo_ensemble_panneau_com qui réuni tout les types de panneau
-- FUNCTION atd16_signalisation.f_i_typ_pann_vigilance()  qui ce trouve sur 160_trigger_panneau_vigilance_com.sql

 -- #################################################################### Trigger #####################################################################
 CREATE TRIGGER t_before_i_typ_pann_vigilance_cdc
    BEFORE INSERT
    ON atd16_signalisation.ngeo_panneau_vigilance_cdc
    FOR EACH ROW
    EXECUTE FUNCTION atd16_signalisation.f_i_typ_pann_vigilance()   ;

-- ##################################################################################################################################################
-- ###                                                   Triggers d'Import du numéro insee du support                                             ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_iu_insee_vigilance_cdc
BEFORE INSERT OR UPDATE 
ON atd16_signalisation.ngeo_panneau_vigilance_cdc
FOR EACH ROW
EXECUTE FUNCTION atd16_signalisation.f_insert_insee();

-- ##################################################################################################################################################
-- ###                                                                                                                                         ###
-- ###                                                          Trigger(s) générique(s)                                                        ###
-- ###                                                                                                                                         ###
-- ##################################################################################################################################################

-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                 ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_i_init_date_creation_pan_vigilance_cdc
  BEFORE INSERT
  ON atd16_signalisation.ngeo_panneau_vigilance_cdc
  FOR EACH ROW
  EXECUTE PROCEDURE atd16_signalisation.f_datecreation() ;
    
-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                       ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_pan_vigilance_cdc
  BEFORE UPDATE ON atd16_signalisation.ngeo_panneau_vigilance_cdc
  FOR EACH ROW EXECUTE PROCEDURE  atd16_signalisation.f_datemaj();