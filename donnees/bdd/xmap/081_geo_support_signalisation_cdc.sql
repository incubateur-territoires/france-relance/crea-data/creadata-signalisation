-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/17 : DC / Création de la table sur Git et des tiggers datemaj et date création
-- 2021/08/18 : DC / Création de la fonction pour le code insee
-- 2022/05/06 : DC / Ajout de plusieurs champs dans les vues des erreurs de support
-- 2022/05/06 : DC / Ajout de plusieurs champs dans la table
-- 2022/05/06 : DC / Création des index
-- 2022/09/15 : DC / Création des vues qui regroupent les panneaux
-- 2022/09/16 : DC / Ajout des commentaires
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###      					          Table géographique : des supports de signalisation/cdc		          			                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.geo_support_signalisation_cdc
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    insee character varying(6), --[ATD16] Code insee de la commune
    ident character varying(45),--[ATD16] numéro du support
    gestionnaire character varying(45),--[ATD16]gestionnaire du support
    origdata character varying(255), --[ATD16] origine de la donnée
    type_support character varying(2), --[ATD16] liste déroulante de la table lst_type_support, defini quel est le support(mât,mur....)
	typologie character varying(2), --[ATD16] liste déroulante de la table lst_typologie_support, defini quel est la typologie pour les supports des panneaux directionnel et de police
    section character varying(2),--[ATD16] liste déroulante de la table lst_section_support, defini quel est la forme du support
	dimension character varying(2),--[ATD16] liste déroulante de la table lst_dimension_support, qui mentionne les dimensions standards
	couleur character varying(50), --[ATD16] Couleur du support
	matiere character varying(100), --[ATD16] Matiére du support(galva,alu...)
	etat_entretien character varying(2), --[ATD16] liste déroulante de la table lst_etat_entretien
    statut character varying(2), --[ATD16] liste déroulante de la table lst_statut, si le support est existant ou souhaité
    date_etat date,--[ATD16] Date à laquelle l'état du support est renseigné
    date_pose date,--[ATD16] Date de pose du support
	fournisseur character varying(255),--[ATD16] Fournisseur du support
    observation character varying(255),--[ATD16] Diverses obsevration sur le support
    url_photo character varying(255),--[ATD16] Photo
    cout_d_achat character varying(15),--[ATD16] Cout d achat du support
	insee_voie_adresse character varying(6), --[ATD16] Code insee de la commune qui va permettre de faire remonter les voies de la commune
    nom_voie character varying(255),--[ATD16] Numéro de la voirie ou ce situe le support en lien avec la table dela voirie communale
	insee_voirie_communale character varying(6), --[ATD16] Code insee de la commune qui va permettre de faire remonter les voies de la commune
    numero_voie character varying(20),--[ATD16] Numero de la voie du support
    date_creation timestamp without time zone,--[ATD16] Date de création du support
    date_maj timestamp without time zone,--[ATD16] date de mise à jour du support
    the_geom geometry, --[ATD16] geometry du positionnement du support

    CONSTRAINT pk_geo_support_signalisation_cdc PRIMARY KEY (gid)
)
WITH
(
OIDS = FALSE
);
ALTER TABLE atd16_signalisation.geo_support_signalisation_cdc
    OWNER to sditecgrp;

-- ################################################################## Commentaires ##################################################################
COMMENT ON TABLE atd16_signalisation.geo_support_signalisation_cdc IS '[ATD16] Table géographique des supports de panneaux de signalisations';

COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.gid IS '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.insee IS '[ATD16] code insee de la commune';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.ident IS '[ATD16] numéro du support';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.gestionnaire IS '[ATD16] Gestionnaire du panneau';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.origdata IS '[ATD16] Origine de la donnée';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.type_support IS '[ATD16] liste déroulante de la table lst_type_support, defini quel est le support(mât,mur....)';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.typologie IS '[ATD16] liste déroulante de la table lst_typologie_support, defini quel est la typologie pour les supports des panneaux directionnel et de police';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.section IS '[ATD16] liste déroulante de la table lst_section_support, defini quel est la forme du support';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.dimension IS '[ATD16] liste déroulante de la table lst_dimension_support, qui mentionne les dimensions standards';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.couleur IS '[ATD16] Couleur du support';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.matiere IS '[ATD16] Matiére du support(galva,alu...)';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.etat_entretien IS '[ATD16] liste déroulante de la table lst_etat_entretien';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.statut IS '[ATD16] liste déroulante de la table lst_statut, si le support est existant ou souhaité';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.date_etat IS '[ATD16] Date à laquelle l''état du support est renseigné';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.date_pose IS '[ATD16] Date de pose du support';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.fournisseur IS '[ATD16] Fournisseur du support';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.observation IS '[ATD16] Diverses observations';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.url_photo IS '[ATD16] Photo';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.cout_d_achat IS '[ATD16] Cout d achat du support';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.insee_voie_adresse IS '[ATD16] Code insee de la commune qui va permettre de faire remonter les voies de la commune';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.nom_voie IS '[ATD16] Numéro de la voirie ou ce situe le support en lien avec la table dela voirie communale';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.insee_voirie_communale IS '[ATD16] Code insee de la commune qui va permettre de faire remonter les voies de la commune';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.numero_voie IS '[ATD16] Numero de la voie du support';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.date_creation IS '[ATD16] Date de création du panneau';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.date_maj IS '[ATD16] Date de mise à jour du panneau';
COMMENT ON COLUMN atd16_signalisation.geo_support_signalisation_cdc.the_geom IS '[ATD16] geometry du positionnement du support';
         
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###   v_geo_support_ss_panneau_cdc : vue permettant une visibilité  des supports qui n'ont pas de panneau associé, complète en flux            ###
-- ##################################################################################################################################################

	
CREATE OR REPLACE VIEW atd16_signalisation.v_geo_support_ss_panneau_cdc
 AS
 SELECT s.gid,
	s.the_geom,
	s.insee,
	s.ident,
	di.panneau_principal as dir_prin,
	ifl.panneau_principal as ifl_prin,
	pp.panneau_principal as pp_prin,
	vi.panneau_principal as vi_prin,
	ad.panneau_principal as ad_prin
		
	FROM atd16_signalisation.geo_support_signalisation_cdc s
   
    LEFT JOIN atd16_signalisation.ngeo_panneau_directionnel_cdc di ON s.gid = di.id_support
    LEFT JOIN atd16_signalisation.ngeo_panneau_info_local_cdc ifl ON s.gid = ifl.id_support
	LEFT JOIN atd16_signalisation.ngeo_panneau_police_cdc pp ON s.gid = pp.id_support
	LEFT JOIN atd16_signalisation.ngeo_panneau_vigilance_cdc vi ON s.gid = vi.id_support
	LEFT JOIN atd16_signalisation.ngeo_panneau_adresse_cdc ad ON s.gid = ad.id_support
	
where di.panneau_principal is null and ifl.panneau_principal is null and pp.panneau_principal is null and vi.panneau_principal is null and ad.panneau_principal is null;


ALTER TABLE atd16_signalisation.v_geo_support_ss_panneau_cdc
OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_signalisation.v_geo_support_ss_panneau_cdc TO sditecgrp;


-- ##################################################################################################################################################
-- ###   v_geo_support_panneau_2_type_cdc : vue permettant une visibilité  des supports qui ont deux types panneau associé en flux                ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_geo_support_panneau_2_type_cdc
AS
SELECT s.gid,
	s.the_geom,
	s.insee,
	s.ident,
	di.ident as numero_directionnel,
	ifl.ident as numero_local,
	pp.ident as numero_police,
	vi.ident as numero_vigilance,
	ad.ident as numero_adresse,
	di.id_support as dir_gid,
	ifl.id_support as ifl_gid,
	pp.id_support as pp_gid,
	vi.id_support as vi_gid,
	ad.id_support as ad_gid
	
	
	FROM atd16_signalisation.geo_support_signalisation_cdc s
   
    LEFT JOIN atd16_signalisation.ngeo_panneau_directionnel_cdc di ON s.gid = di.id_support
    LEFT JOIN atd16_signalisation.ngeo_panneau_info_local_cdc ifl ON s.gid = ifl.id_support
	LEFT JOIN atd16_signalisation.ngeo_panneau_police_cdc pp ON s.gid = pp.id_support
	LEFT JOIN atd16_signalisation.ngeo_panneau_vigilance_cdc vi ON s.gid = vi.id_support
	LEFT JOIN atd16_signalisation.ngeo_panneau_adresse_cdc ad ON s.gid = ad.id_support
	
where di.id_support = ifl.id_support or pp.id_support = di.id_support or pp.id_support =  ifl.id_support 
or vi.id_support = di.id_support or  vi.id_support = ifl.id_support or vi.id_support = pp.id_support or vi.id_support = ad.id_support
or di.id_support = ad.id_support or ifl.id_support = ad.id_support  or pp.id_support = ad.id_support;
	 
ALTER TABLE atd16_signalisation.v_geo_support_panneau_2_type_cdc
OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_signalisation.v_geo_support_panneau_2_type_cdc TO sditecgrp;

-- ##################################################################################################################################################
-- ### v_geo_support_2_panneaux_principaux_com : vue permettant une visibilité  des supports qui ont deux panneaux principaux en flux             ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_geo_support_2_panneaux_principaux_cdc
 AS
SELECT s.gid,
    s.the_geom,
	s.insee,
	s.ident,
	d.ident as numero_directionnel,
	i.ident as numero_local,
	p.ident as numero_police,
	v.ident as numero_vigilance,
	a.ident as numero_adresse,
	--[ATD16] Nombre de panneau principaux de type police
    CASE WHEN sum(p.panneau_principal) IS NULL THEN '0' ELSE sum(p.panneau_principal) END - count(p.panneau_principal) AS nb_prio_police,
	--[ATD16] Nombre de panneau principaux de type directionnel
    CASE WHEN sum(d.panneau_principal) IS NULL THEN '0' ELSE sum(d.panneau_principal) END - count(d.panneau_principal) AS nb_prio_directionnel, 
	
	--[ATD16] Nombre de panneau principaux de type information local
    CASE WHEN sum(i.panneau_principal) IS NULL THEN '0' ELSE  sum(i.panneau_principal) END - count(i.panneau_principal) AS nb_prio_info, 
	--[ATD16] Nombre de panneau principaux de type adresse
    CASE WHEN sum(a.panneau_principal) IS NULL THEN '0' ELSE  sum(a.panneau_principal) END - count(a.panneau_principal) AS nb_prio_adresse, 
	--[ATD16] Nombre de panneau principaux de type vigilance
    CASE WHEN sum(v.panneau_principal) IS NULL THEN '0' ELSE  sum(v.panneau_principal) END - count(v.panneau_principal) AS nb_prio_vigilance, 
	--[ATD16] Nombre de panneau principaux total
    (CASE WHEN sum(p.panneau_principal) IS NULL THEN '0' ELSE sum(p.panneau_principal) END - count(p.panneau_principal) + 
	CASE WHEN sum(d.panneau_principal) IS NULL THEN '0' ELSE sum(d.panneau_principal) END - count(d.panneau_principal) + 
	CASE WHEN sum(i.panneau_principal) IS NULL THEN '0' ELSE  sum(i.panneau_principal) END - count(i.panneau_principal)+
	CASE WHEN sum(a.panneau_principal) IS NULL THEN '0' ELSE  sum(a.panneau_principal) END - count(a.panneau_principal)+
	CASE WHEN sum(v.panneau_principal) IS NULL THEN '0' ELSE  sum(v.panneau_principal) END - count(v.panneau_principal)) AS nb_panneau_principal 
	FROM atd16_signalisation.geo_support_signalisation_cdc s
    LEFT JOIN atd16_signalisation.ngeo_panneau_police_cdc p ON s.gid = p.id_support
    LEFT JOIN atd16_signalisation.ngeo_panneau_directionnel_cdc d ON s.gid = d.id_support
    LEFT JOIN atd16_signalisation.ngeo_panneau_info_local_cdc i ON s.gid = i.id_support
	LEFT JOIN atd16_signalisation.ngeo_panneau_adresse_cdc a ON s.gid = a.id_support
	LEFT JOIN atd16_signalisation.ngeo_panneau_vigilance_cdc v ON s.gid = v.id_support
	
	GROUP BY s.gid,d.ident,i.ident,p.ident,v.ident,a.ident
	--[ATD16]Garde les valeurs qui ont plus d'un panneau principal sur le même support
	HAVING (CASE WHEN sum(p.panneau_principal) IS NULL THEN '0' ELSE sum(p.panneau_principal) END - count(p.panneau_principal) + 
	CASE WHEN sum(d.panneau_principal) IS NULL THEN '0' ELSE sum(d.panneau_principal) END - count(d.panneau_principal) + 
	CASE WHEN sum(i.panneau_principal) IS NULL THEN '0' ELSE  sum(i.panneau_principal) END - count(i.panneau_principal)+
	CASE WHEN sum(a.panneau_principal) IS NULL THEN '0' ELSE  sum(a.panneau_principal) END - count(a.panneau_principal)+
	CASE WHEN sum(v.panneau_principal) IS NULL THEN '0' ELSE  sum(v.panneau_principal) END - count(v.panneau_principal)) > 1 
	ORDER BY s.gid;

ALTER TABLE atd16_signalisation.v_geo_support_2_panneaux_principaux_cdc
OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_signalisation.v_geo_support_2_panneaux_principaux_cdc TO sditecgrp;

-- ##################################################################################################################################################
-- ###                 v_geo_pann_police_com : vue permettant l'affichage de tous les panneaux police en flux                                     ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_geo_pann_police_cdc AS 
 SELECT
	s.gid as gid_s,
	s.ident AS ident_support,
	s.insee,
	s.the_geom,
	s.nom_voie,
	s.numero_voie,
	pp.gid ,
	pp.id_support,
	pp.statut,
	pp.panneau_principal,
	pp.type_pann,
	lst.libelle,
	lst.code_pann,
	lst.type,
	pp.ident,
	pp.nature,
	pp.date_pose,
	pp.etat_entretien,
	pp.date_etat,
	pp.gestionnaire,
	pp.observations,
	pp.taille,
    pp.retroreflechissant,
    pp.classe_reflechissante,
	pp.fournisseur,
    pp.cout,
	s.url_photo,
	pp.origdata
	FROM atd16_signalisation.ngeo_panneau_police_cdc pp
	LEFT JOIN atd16_signalisation.geo_support_signalisation_cdc s ON s.gid = pp.id_support
	LEFT JOIN atd16_signalisation.lst_type_police lst ON lst.code = pp.type_pann
	WHERE pp.panneau_principal ='2';--Permet d'avoir à l'affichage seulement les panneaux principaux

ALTER TABLE atd16_signalisation.v_geo_pann_police_cdc
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.v_geo_pann_police_cdc TO sditecgrp;

-- ##################################################################################################################################################
-- ###                 v_geo_pann_directionnel_com : vue permettant une visibilité  de tous les panneaux directionnel en flux                     ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_geo_pann_directionnel_cdc AS 
SELECT
	s.gid as gid_s,
	s.ident ident_support,
	s.insee,
	s.the_geom,
	s.nom_voie,
	s.numero_voie,	
	di.gid,
	di.id_support,
	di.statut,
	di.panneau_principal,
	di.type_pann,
	lst.libelle,
	lst.code_pann,
	lst.type,
	di.ident,
	di.nature,
	di.date_pose,
	di.fleche,
	di.etat_entretien,
	di.date_etat,
	di.gestionnaire,
	di.observations,
	s.url_photo,
	di.taille,
    di.retroreflechissant,
    di.classe_reflechissante,
	di.fournisseur,
    di.cout,
	di.origdata
	FROM atd16_signalisation.ngeo_panneau_directionnel_cdc di
	LEFT JOIN atd16_signalisation.geo_support_signalisation_cdc s ON s.gid = di.id_support
	LEFT JOIN atd16_signalisation.lst_type_directionnel lst ON lst.code = di.type_pann 
	WHERE di.panneau_principal ='2'--Permet d'avoir à l'affichage seulement les panneaux principaux
	;

ALTER TABLE atd16_signalisation.v_geo_pann_directionnel_cdc
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.v_geo_pann_directionnel_cdc TO sditecgrp;


-- ##################################################################################################################################################
-- ###             v_geo_pann_info_local_com : vue permettant une visibilité  de tous les panneaux information local en flux                      ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_geo_pann_info_local_cdc AS 
SELECT
	s.gid as gid_s,
	s.ident ident_support,
	s.insee,
	s.the_geom,
	s.nom_voie,
	s.numero_voie,	
	ifl.gid ,
	ifl.id_support,
	ifl.statut,
	ifl.panneau_principal,
	ifl.type_pann,
	lst.libelle,
	lst.code_pann,
	lst.type,
	ifl.ident,
	ifl.nature,
	ifl.date_pose,
	ifl.fleche,
	ifl.etat_entretien,
	ifl.date_etat,
	ifl.nom_entreprise,
	ifl.gestionnaire,
	ifl.observations,
	ifl.taille,
    ifl.retroreflechissant,
    ifl.classe_reflechissante,
	ifl.fournisseur,
    ifl.cout,
	s.url_photo,
	ifl.origdata
	FROM atd16_signalisation.ngeo_panneau_info_local_cdc ifl
	LEFT JOIN atd16_signalisation.geo_support_signalisation_cdc s ON s.gid = ifl.id_support
	LEFT JOIN atd16_signalisation.lst_type_inf_local lst ON lst.code = ifl.type_pann
	WHERE ifl.panneau_principal ='2'--Permet d'avoir à l'affichage seulement les panneaux principaux
	;


ALTER TABLE atd16_signalisation.v_geo_pann_info_local_cdc
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.v_geo_pann_info_local_cdc TO sditecgrp;

-- ##################################################################################################################################################
-- ###             v_geo_pann_info_local_com : vue permettant une visibilité  de tous les panneaux de vigilance en flux                           ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_geo_panneau_vigilance_cdc
 AS
 SELECT s.gid AS gid_s,
    s.ident AS ident_support,
    s.insee,
    s.the_geom,
    s.nom_voie,
    s.numero_voie,
    vi.gid,
    vi.id_support,
    vi.statut,
	vi.panneau_principal,
    vi.ident,
    vi.nature,
    vi.date_pose,
    vi.etat_entretien,
    vi.date_etat,
    vi.gestionnaire,
    vi.observations,
	vi.taille,
    vi.retroreflechissant,
    vi.classe_reflechissante,
	vi.fournisseur,
    vi.cout,
    vi.origdata
   FROM atd16_signalisation.ngeo_panneau_vigilance_cdc vi
     LEFT JOIN atd16_signalisation.geo_support_signalisation_cdc s ON s.gid = vi.id_support;
     

ALTER TABLE atd16_signalisation.v_geo_panneau_vigilance_cdc
    OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_signalisation.v_geo_pann_directionnel_cdc TO sditecgrp;

-- ##################################################################################################################################################
-- ###             v_geo_pann_info_local_com : vue permettant une visibilité  de tous les panneaux de nom de voie en flux                         ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_geo_pann_adresse_cdc AS 
 SELECT
	s.gid as gid_s,
	s.ident AS ident_support,
	s.insee,
	s.the_geom,
	s.nom_voie,
	s.numero_voie,
	pa.gid ,
	pa.id_support,
	pa.statut,
	pa.panneau_principal,
	pa.ident,
	pa.nature,
	pa.date_pose,
	pa.etat_entretien,
	pa.date_etat,
	pa.gestionnaire,
	pa.observations,
	pa.taille,
    pa.retroreflechissant,
    pa.classe_reflechissante,
	pa.fournisseur,
    pa.cout,
	pa.origdata
	FROM atd16_signalisation.ngeo_panneau_adresse_cdc pa
	LEFT JOIN atd16_signalisation.geo_support_signalisation_cdc s ON s.gid = pa.id_support
	WHERE pa.panneau_principal ='2';--Permet d'avoir à l'affichage seulement les panneaux principaux


ALTER TABLE atd16_signalisation.v_geo_pann_adresse_cdc
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.v_geo_pann_adresse_cdc TO sditecgrp;

-- ##################################################################################################################################################
-- ### 		v_ngeo_ensemble_panneau_cdc : vue non geographique regroupant les différents panneaux qui sert pour v_geo_ensemble_panneau_cdc  	  ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_ngeo_ensemble_panneau_cdc AS 
    SELECT 
	gid,id_support,statut,panneau_principal,ident,nature,date_pose,etat_entretien,date_etat,gestionnaire,observations,taille,retroreflechissant,classe_reflechissante,
	fournisseur,cout,panneau_type 
	FROM atd16_signalisation.ngeo_panneau_police_cdc
    UNION
    SELECT
	gid,id_support,statut,panneau_principal,ident,nature,date_pose,etat_entretien,date_etat,gestionnaire,observations,taille,retroreflechissant,classe_reflechissante,
	fournisseur,cout,panneau_type 
	FROM atd16_signalisation.ngeo_panneau_directionnel_cdc
    UNION
    SELECT
	gid,id_support,statut,panneau_principal,ident,nature,date_pose,etat_entretien,date_etat,gestionnaire,observations,taille,retroreflechissant,classe_reflechissante,
    fournisseur,cout,panneau_type 
	FROM atd16_signalisation.ngeo_panneau_info_local_cdc
    UNION
    SELECT
	gid,id_support,statut,panneau_principal,ident,nature,date_pose,etat_entretien,date_etat,gestionnaire,observations,taille,retroreflechissant,classe_reflechissante,
	fournisseur,cout,panneau_type 
	FROM atd16_signalisation.ngeo_panneau_vigilance_cdc
    UNION
    SELECT
	gid,id_support,statut,panneau_principal,ident,nature,date_pose,etat_entretien,date_etat,gestionnaire,observations,taille,retroreflechissant,classe_reflechissante,
	fournisseur,cout,panneau_type 
	FROM atd16_signalisation.ngeo_panneau_adresse_cdc;

ALTER TABLE atd16_signalisation.v_ngeo_ensemble_panneau_cdc
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.v_ngeo_ensemble_panneau_cdc TO sditecgrp;

-- ##################################################################################################################################################
-- ### 							v_geo_ensemble_panneau_cdc : vue permettant une visibilité  de tous les panneaux     							  ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_signalisation.v_geo_ensemble_panneau_cdc AS 
    SELECT 
    s.gid as gid_s,
	s.ident AS ident_support,
	s.insee,
	s.the_geom,
	s.nom_voie,
	s.numero_voie,
	ep.gid,
    ep.id_support,
    ep.statut,
    ep.panneau_principal,
    ep.ident,nature,
    ep.date_pose,
    ep.etat_entretien,
    ep.date_etat,
    ep.gestionnaire,
    ep.observations,
    ep.taille,
    ep.retroreflechissant,
    ep.classe_reflechissante,
	ep.fournisseur,
    ep.cout,
    ep.panneau_type 
	FROM atd16_signalisation.v_ngeo_ensemble_panneau_cdc ep
    LEFT JOIN atd16_signalisation.geo_support_signalisation_cdc s ON s.gid = ep.id_support
    

ALTER TABLE atd16_signalisation.v_geo_ensemble_panneau_cdc
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.v_geo_ensemble_panneau_cdc TO sditecgrp;

-- ###################################################################### INDEX #####################################################################

-- DROP INDEX IF EXISTS atd16_signalisation.idx_geo_support_signalisation_cdc_insee;

CREATE INDEX idx_geo_support_signalisation_cdc_insee
  ON atd16_signalisation.geo_support_signalisation_cdc USING btree
  (insee COLLATE pg_catalog."default");

-- DROP INDEX IF EXISTS atd16_signalisation.idx_geo_support_signalisation_cdc_the_geom_gist;

CREATE INDEX idx_geo_support_signalisation_cdc_the_geom_gist
  ON atd16_signalisation.geo_support_signalisation_cdc USING gist
  (the_geom);

-- DROP INDEX IF EXISTS atd16_signalisation.idx_geo_support_signalisation_cdc_ident;

CREATE INDEX idx_geo_support_signalisation_cdc_ident
  ON atd16_signalisation.geo_support_signalisation_cdc USING btree
  (ident COLLATE pg_catalog."default");