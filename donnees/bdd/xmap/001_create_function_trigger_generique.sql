-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/21 : DC / cration des fonction date_creation et datemaj

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

CREATE OR REPLACE FUNCTION atd16_signalisation.f_datecreation()   
RETURNS TRIGGER AS $$
BEGIN
    NEW.date_creation = now();
    RETURN NEW;   
END;
$$ language 'plpgsql';

ALTER FUNCTION atd16_signalisation.f_datecreation()
  OWNER TO sditecgrp;
    
-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE FUNCTION atd16_signalisation.f_datemaj()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.date_maj = now();
    RETURN NEW;   
END;
$BODY$;

ALTER FUNCTION atd16_signalisation.f_datemaj()
    OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_signalisation.f_datemaj() TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_signalisation.f_datemaj() TO simapdatagrp;

GRANT EXECUTE ON FUNCTION atd16_signalisation.f_datemaj() TO PUBLIC;