-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/06 : DC / Création de la table

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table liste : table des classes de rétroréflechissant des panneaux				                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_classe_retroreflechissant 
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
  code integer, --[ATD16]Code de la classe
  libelle character varying(255),  --[ATD16]Valeur de la classe
  "order" integer,  --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_classe_retroreflechissant  PRIMARY KEY (gid)
)
WITH 
(
  OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_classe_retroreflechissant
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_classe_retroreflechissant TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_classe_retroreflechissant (code, libelle,"order")
VALUES
('01','Classe 1','1'),
('02','Classe 2','2'),
('03','Classe 3','3');