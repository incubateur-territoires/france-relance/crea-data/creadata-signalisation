-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/30 : DC / Création de la table sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table non géographique : Domaine de valeur des statuts des supports et des panneaux                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_statut 
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
  code integer, --[ATD16]Code du statut
  libelle character varying(255),  --[ATD16]Valeur du statut
  "order" integer,  --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_statut PRIMARY KEY (gid)
)
WITH 
(
  OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_statut
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_statut TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_statut (code, libelle,"order")
VALUES
('00','','1'),
('01','Existant','2'),
('02','Souhaité','3');