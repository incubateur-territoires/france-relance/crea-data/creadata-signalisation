-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/30 : DC / Création de la table sur Git
-- 2022/09/15 : DC / Sert aussi pour indiquer si le panneau est retroreflechissant.

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                               Table non géographique : Domaine de valeur des panneaux principaux                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_principal 
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
  code integer, --[ATD16]Code de la valeur des panneaux principaux
  libelle character varying(255) , --[ATD16]Valeur des panneaux principaux
  "order" integer, --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_principal  PRIMARY KEY (gid)
)
WITH 
(
OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_principal
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_principal  TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_principal   (code, libelle, "order")
VALUES

('02','Oui','1'),
('01','Non','2');