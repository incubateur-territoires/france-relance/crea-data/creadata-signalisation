-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/02 : DC / Création de la table des panneaux d adresse
-- 2021/07/02 : DC / Création des triggers date de création et date de mise à jour
--2021/07/13 : DC / Modification compléte de la table pour la liée aux supports
-- 2021/08/18 : DC Création du trigger d import du code insee
-- 2022/05/10 : DC Création des champs taille, retroreflechissant,classe_reflechissante,fournisseur,cout
-- 2022/09/15 : DC Ajout du champs panneau_type
-- 2022/09/16 : DC / Ajout des commentaires

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table géographique : Des panneaux de nom de rue                                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.ngeo_panneau_adresse_com
(
    gid SERIAL NOT NULL,--[PK][ATD16] Identifiant unique généré automatiquement
    insee character varying(6),--[ATD16] Code insee de la commune
    id_support integer,--[ATD16] Identifiant du support du panneau
    statut integer,--[ATD16] Liste deroulante lst_statut, code du statut
    panneau_principal integer,
    ident character varying(255),--[ATD16] Numéro du panneau
    nature integer,--[ATD16] Liste deroulante lst_nature, code su statut
	taille integer, --[ATD16] Liste deroulante de la table lst_taille qui signifie la taille du panneau
	retroreflechissant integer, --[ATD16]Le panneau est il retroreflechissan
	classe_reflechissante integer, --[ATD16] Liste déroulante de la table classe retroreflechissant 
    date_pose date,--[ATD16] Date de pose du panneau
    etat_entretien integer,--[ATD16] Liste deroulante lst_etat_entretien, code de l'état
    date_etat date,--[ATD16] Date à laquelle l'état est notifié
    gestionnaire character varying(45),--[ATD16] Gestionnaire du panneau
	fournisseur character varying(45),--[ATD16] Fourb=nisseur du panneau
	cout character varying(10),--[ATD16] prix d'achat du panneau
    observations character varying(255),--[ATD16] Diverses observations
    origdata character varying(255),--[ATD16] origine de la donnée
    panneau_type character varying(50) --Mets par defaut le type de panneau va servir pour la vue des qui rassemble tout les panneaux
    date_creation timestamp without time zone,--[ATD16] Date de création du panneau
    date_maj timestamp without time zone,--[ATD16] Date de mise à jour du panneau
    CONSTRAINT pk_ngeo_panneau_adresse_com PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE atd16_signalisation.ngeo_panneau_adresse_com
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_signalisation.ngeo_panneau_adresse_com TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.ngeo_panneau_adresse_com TO simapdatagrp;
GRANT ALL ON TABLE atd16_signalisation.ngeo_panneau_adresse_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################
COMMENT ON TABLE atd16_signalisation.ngeo_panneau_adresse_com IS '[ATD16] Table non géographique listant les indormations des panneaux des noms de voie';

COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.gid IS '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.insee IS '[ATD16] code insee de la commune';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.id_support IS '[ATD16] Identifiant du support du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.statut IS '[ATD16] Liste deroulante lst_statut, code du statut';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.panneau_principal IS '[ATD16] Numéro du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.ident IS '[ATD16] Numéro du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.nature IS '[ATD16] Liste deroulante lst_nature, code de la nature';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.taille IS '[ATD16] Liste deroulante de la table lst_taille qui signifie la taille du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.retroreflechissant IS '[ATD16]Le panneau est il retroreflechissant';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.classe_reflechissante IS '[ATD16] Liste déroulante de la table classe retroreflechissant'; 
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.date_pose IS '[ATD16] Date de pose du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.etat_entretien IS '[ATD16] Liste deroulante lst_etat_entretien, code de l''état';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.date_etat IS '[ATD16] Date à laquelle l''état est notifié';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.gestionnaire IS '[ATD16] Gestionnaire du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.fournisseur IS '[ATD16] Fourb=nisseur du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.cout IS '[ATD16] prix d''achat du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.observations IS '[ATD16] Diverses observations';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.origdata IS '[ATD16] Origine de la donnée';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.panneau_type IS '[ATD16] Mets par defaut le type de panneau va servir pour la vue des qui rassemble tout les panneaux';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.date_creation IS '[ATD16] Date de création du panneau';
COMMENT ON COLUMN atd16_signalisation.ngeo_panneau_adresse_com.date_maj IS '[ATD16] Date de mise à jour du panneau';