-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/02 : DC / Création de la fonction et du trigger de suppression des panneaux
-- 2022/09/26 : DC Création de la fonction pour le champ type panneau

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table ngeo_panneau_police                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- ##################################################################################################################################################
-- ###                                      Supression des panneaux de police suite à la supression du support                                    ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################
CREATE OR REPLACE FUNCTION  atd16_signalisation.f_delete_pan_police_com()

RETURNS trigger AS

$BODY$ 

BEGIN
DELETE FROM atd16_signalisation.ngeo_panneau_police_com
WHERE (id_support) = (OLD.gid) ;
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION atd16_signalisation.f_delete_pan_police_com()
OWNER TO sditecgrp;
GRANT EXECUTE ON FUNCTION atd16_signalisation.f_delete_pan_police_com() TO public;
GRANT EXECUTE ON FUNCTION atd16_signalisation.f_delete_pan_police_com() TO sditecgrp;

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER d_b_pan_police_com
    AFTER DELETE
    ON atd16_signalisation.geo_support_signalisation_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_signalisation.f_delete_pan_police_com();

-- ##################################################################################################################################################
-- ###                                                     Création de la donnée pour le champ type_panneau                                       ###
-- ##################################################################################################################################################

 -- #################################################################### Fonction ####################################################################

-- Fonction qui indique dans la table de quel type de panneau il s'agit. Et cette valeur sera utile dans la vue v_geo_ensemble_panneau_com qui réuni tout les types de panneau
CREATE OR REPLACE FUNCTION atd16_signalisation.f_i_typ_pann_police()   
RETURNS TRIGGER AS $$
BEGIN
    NEW.panneau_type = 'Panneau Police';
    RETURN NEW;   
END;
$$ language 'plpgsql';

ALTER FUNCTION atd16_signalisation.f_i_typ_pann_police()
  OWNER TO sditecgrp;

 -- #################################################################### Trigger #####################################################################
 CREATE TRIGGER t_before_i_typ_pann_police_com
    BEFORE INSERT
    ON atd16_signalisation.ngeo_panneau_police_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_signalisation.f_i_typ_pann_police()   ;

-- ##################################################################################################################################################
-- ###                                                   Triggers d'Import du numéro insee du support                                             ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_iu_insee_police_com
BEFORE INSERT OR UPDATE 
ON atd16_signalisation.ngeo_panneau_police_com
FOR EACH ROW
EXECUTE FUNCTION atd16_signalisation.f_insert_insee_com();

-- ##################################################################################################################################################
-- ###                                                                                                                                         ###
-- ###                                                          Trigger(s) générique(s)                                                        ###
-- ###                                                                                                                                         ###
--##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                 ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_i_init_date_creation_police_com
BEFORE INSERT
ON atd16_signalisation.ngeo_panneau_police_com
FOR EACH ROW
EXECUTE PROCEDURE atd16_signalisation.f_datecreation() ;

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                     ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_police_com
BEFORE INSERT
ON atd16_signalisation.ngeo_panneau_police_com
FOR EACH ROW
EXECUTE PROCEDURE atd16_signalisation.f_datemaj();