-- ################################################################# SUIVI CODE SQL #################################################################

-- 2025/05/04 : DC / Création de la table sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table non géographique : Domaine de valeur des typologies des panneaux			                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################
--Indique pour quelle type de panneau le support est destinée (Police,directionnel)

CREATE TABLE atd16_signalisation.lst_typologie_support 
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
  code integer, --[ATD16]Code de la typologie
  type character varying(3), --[ATD16]Abréviation de la typologie
  libelle character varying(255),  --[ATD16]Valeur de la typologie
  "order" integer,  --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_typologie_support PRIMARY KEY (gid)
)
WITH 
(
  OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_typologie_support
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_typologie_support TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_typologie_support (code,type, libelle,"order")
VALUES
('00','SP','Signalisation Permanente de Police','1'),
('01','SD1','Signalisation Directionnelle Permanente','2'),
('02','SD2','Signalisation Directionnelle Permanente','3'),
('03','SD3','Signalisation Directionnelle Permanente','4'),
('04','TP','Signalisation Temporaire de Police','5'),
('05','TD','Signalisation Temporaire Directionnelle','6');