-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/05 : DC / Création du fichier

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                       Fonctions triggers et triggers spécifiques à la table geo_support_signalisation_com sup                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- ##################################################################################################################################################
-- ###                                                   Initialisation du code insee dans les panneaux                                           ###
-- ##################################################################################################################################################

--Permet de faire remonter le code insee du support dans le panneau qu il lui est associé

CREATE FUNCTION atd16_signalisation.f_insert_insee_com()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.insee = -- Le numéro insee du panneau  créé ou modifié est égal à
    (SELECT sup.insee --au numéro insee du support
    FROM atd16_signalisation.geo_support_signalisation_com sup
    WHERE NEW.id_support = sup.gid); -- sélection de la ligne nouvellement créée
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION atd16_signalisation.f_insert_insee_com()
    OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_signalisation.f_insert_insee_com() TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_signalisation.f_insert_insee_com() TO PUBLIC;

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_i_init_date_creation_support_com
BEFORE INSERT ON atd16_signalisation.geo_support_signalisation_com 
FOR EACH ROW
EXECUTE PROCEDURE  atd16_signalisation.f_datecreation();

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_support_com
    BEFORE UPDATE 
    ON atd16_signalisation.geo_support_signalisation_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_signalisation.f_datemaj();