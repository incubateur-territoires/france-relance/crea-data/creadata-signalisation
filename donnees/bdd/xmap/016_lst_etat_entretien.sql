-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/30 : DC / Création de la table sur Git
-- 2022/05/04 : DC / Ajout de la valeur moyen
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###             Table non géographique : Domaine de valeur de l'état d'entretiens des supports et des panneaux                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_etat_entretien
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement 
  code integer, --[ATD16]Code de l'état d'entretien 
  libelle character varying(255), --[ATD16]Valeur de l'état d'entreti
  "order" integer,  --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_etat_entretien PRIMARY KEY (gid)
)
WITH
(
OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_etat_entretien
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_etat_entretien TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_etat_entretien (code, libelle,"order")
VALUES
('00','NC','2'),
('01','Bon','3'),
('02','Moyen','4'),
('03','Mauvais','5');