-- ################################################################# SUIVI CODE SQL #################################################################

-- 2025/05/04 : DC / Création de la table

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table non géographique : Domaine de valeur des sections des supports 				                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2025/05/04 : DC / Création de la table

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table non géographique : Domaine de valeur des sections des supports 				                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_signalisation.lst_section_support 
(
  gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
  code integer, --[ATD16]Code de la section
  libelle character varying(255),  --[ATD16]Valeur de la section
  "order" integer,  --[ATD16] ordre des valeurs
  CONSTRAINT pk_lst_section_support PRIMARY KEY (gid)
)
WITH 
(
  OIDS=FALSE
);
ALTER TABLE atd16_signalisation.lst_section_support
OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_signalisation.lst_section_support TO sditecgrp;

-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_signalisation.lst_section_support (code,libelle,"order")
VALUES
('01','Rond lisse','1'),
('02','Rond cannelé','2'),
('03','Carré','3'),
('04','Rectangle','4'),
('05','En IPN','5'),
('99','Autre','99');