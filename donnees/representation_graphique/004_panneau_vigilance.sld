<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des panneaux de la vigilance citoyenne de la signalisation verticale'
    appliquée à la couche v_geo_panneau_vigilance:v_geo_panneau_vigilance(Geoserver) ; Signalisation/Signalisation Verticale/Thème signalétique/Panneau de vigilance citoyenne (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:se="http://www.opengis.net/se" version="1.1.0" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <se:Name>v_geo_panneau_vigilance</se:Name>
    <UserStyle>
      <se:Name>v_geo_panneau_vigilance</se:Name>
      <se:FeatureTypeStyle>
	  		<se:Rule>
          <se:Name>Panneau vigilance citoyenne</se:Name>
          <se:Description>
            <se:Title>Panneau vigilance citoyenne</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
			<ogc:PropertyIsEqualTo>
              <ogc:PropertyName>panneau_principal</ogc:PropertyName>
              <ogc:Literal>2</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  <se:MinScaleDenominator>0</se:MinScaleDenominator>
          <se:MaxScaleDenominator>300</se:MaxScaleDenominator>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/signalisation/autre/vigilance_citoyenne.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>45</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
       </se:Rule>
		<se:Rule>
          <se:Name>Panneau vigilance citoyenne</se:Name>
          <se:Description>
            <se:Title>Panneau vigilance citoyenne</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
			<ogc:PropertyIsEqualTo>
              <ogc:PropertyName>panneau_principal</ogc:PropertyName>
              <ogc:Literal>2</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  <se:MinScaleDenominator>301</se:MinScaleDenominator>
          <se:MaxScaleDenominator>750</se:MaxScaleDenominator>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/signalisation/autre/vigilance_citoyenne.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>35</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
       </se:Rule>
	   		<se:Rule>
          <se:Name>Panneau vigilance citoyenne</se:Name>
          <se:Description>
            <se:Title>Panneau vigilance citoyenne</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
			<ogc:PropertyIsEqualTo>
              <ogc:PropertyName>panneau_principal</ogc:PropertyName>
              <ogc:Literal>2</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  <se:MinScaleDenominator>751</se:MinScaleDenominator>
          <se:MaxScaleDenominator>1150</se:MaxScaleDenominator>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/signalisation/autre/vigilance_citoyenne.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>25</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
       </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
