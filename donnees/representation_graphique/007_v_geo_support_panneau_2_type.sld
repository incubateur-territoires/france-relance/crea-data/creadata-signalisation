<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des supports avec 2 types de panneau différent de la signalisation verticale'
    appliquée à la couche v_geo_support_panneau_2_type : v_geo_support_panneau_2_type(Geoserver) ; Signalisation/Signalisation Verticale/Erreur support/Support avec 2 types de panneaux (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" version="1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>v_geo_support_panneau_2_type</se:Name>
    <UserStyle>
      <se:Name>v_geo_support_panneau_2_type</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>Single symbol</se:Name>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>square</se:WellKnownName>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#fc0000</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">4</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>21</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
