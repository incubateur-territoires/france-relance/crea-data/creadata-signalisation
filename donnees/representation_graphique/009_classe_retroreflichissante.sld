<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
Représentation thématique des classes rétroréfléchissante des panneaux de la signalisation verticale'
    appliquée à la couche v_geo_ensemble_panneau : v_geo_classe_retroreflechissante(Geoserver) ; Signalisation/Signalisation Verticale/Thème signalétique/ Classe rétroréfléchissante (X'MAP)
-->

<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>Classe rétroréfléchissante</Name>
    <UserStyle>
      <Name>Classe rétroréfléchissante</Name>
      <FeatureTypeStyle>
        <Rule>
          <TextSymbolizer>
			<Label>
				<ogc:PropertyName>classe_reflechissante</ogc:PropertyName>
			</Label>
            <Font>
           		<CssParameter name="font-size">15</CssParameter>
         	</Font>
            <LabelPlacement>
              <PointPlacement>
                <AnchorPoint>
                  <AnchorPointX>0.5</AnchorPointX>
                  <AnchorPointY>-1.5</AnchorPointY>
                </AnchorPoint>
              </PointPlacement>
            </LabelPlacement>
            <Halo>
           		<Radius><ogc:Literal>3</ogc:Literal></Radius>
           		<Fill>
             		<CssParameter name="fill">#FFFFFF</CssParameter>
           		</Fill>
         	</Halo>
            <Fill>
              <CssParameter name="fill">#FE2D00</CssParameter>
            </Fill>
		   </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>